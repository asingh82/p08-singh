//
//  GameScene.swift
//  DotThrough
//
//  Created by Anshima on 10/05/17.
//  Copyright © 2017 AnshimaSingh. All rights reserved.
//

import SpriteKit
import GameplayKit

struct CollideBitMask {
    static let Player:UInt32 = 0x00
    static let Obstacle:UInt32 = 0x01
}

class GameScene: SKScene,SKPhysicsContactDelegate{
    
    var player: SKSpriteNode!
    
    var playerPosition:CGPoint!
    var flag=false
    var scoreLabel : SKLabelNode?
    var scoreValue: Int = 0 {
        didSet {
            print("The value of score is: \(scoreValue)")
            scoreLabel?.text = "Score: \(scoreValue)"
        }
    }
    
        
    
    enum ObstacleSize:Int {
        case small = 0
        case medium = 1
        case large = 2
    }
    
    enum RowType:Int {
        case oneS = 0
        case oneM = 1
        case oneL = 2
        case twoS = 3
        case twoM = 4
        case threeS = 5
        case fourS = 6
    }

   
   

    let right = SKAction.moveBy(x: 150, y: 0, duration: 0.4)
    let left = SKAction.moveBy(x: -150, y: 0, duration: 0.4)
    
    func swipedRight(sender:UISwipeGestureRecognizer){
        player.run(right)
    }
    func swipedLeft(sender:UISwipeGestureRecognizer){
        player.run(left)
    }

    
    func addPlayer() {
         player = SKSpriteNode(color: UIColor.yellow, size: CGSize(width: 50, height: 50))
        player.position = CGPoint(x: self.size.width / 2, y: 700)
        player.name = "PLAYER"
        player.physicsBody?.isDynamic = false
        player.physicsBody = SKPhysicsBody(rectangleOf: player.size)
        player.physicsBody?.categoryBitMask = CollideBitMask.Player
        player.physicsBody?.collisionBitMask = 0
        player.physicsBody?.contactTestBitMask = CollideBitMask.Obstacle
        addChild(player)
        
        playerPosition = player.position
        
    }
    
    func addObstacle (_ type:ObstacleSize) -> SKSpriteNode {
        let obstacle = SKSpriteNode(color: UIColor.yellow, size: CGSize(width: 0, height: 30))
        obstacle.name = "OBSTACLE"
        obstacle.physicsBody?.isDynamic = true
        obstacle.colorBlendFactor = 1.0
        if(!flag){
            scoreValue = scoreValue + 1
        }
        switch type {
        case .small:
            obstacle.size.width = self.size.width * 0.15
            break
        case .medium:
            obstacle.size.width = self.size.width * 0.30
            break
        case .large:
            obstacle.size.width = self.size.width * 0.80
            break
            
        }
        
        obstacle.position = CGPoint(x: 0, y: self.size.height + obstacle.size.height)
        obstacle.physicsBody = SKPhysicsBody(rectangleOf: obstacle.size)
        obstacle.physicsBody?.categoryBitMask = CollideBitMask.Obstacle
        obstacle.physicsBody?.collisionBitMask = 0
        
        return obstacle
        
    }
    
    func addMovement (_ obstacle:SKSpriteNode) {
        var obstacleArray = [SKAction]()
        
        obstacleArray.append(SKAction.move(to: CGPoint(x: obstacle.position.x/2, y: -obstacle.size.height), duration: TimeInterval(4)))
        obstacleArray.append(SKAction.removeFromParent())
        
        obstacle.run(SKAction.sequence(obstacleArray))
    }
    
    func addRowObs (_ type:RowType) {
        switch type {
        case .oneS:
            let obs = addObstacle(.small)
            obs.position = CGPoint(x: self.size.width / 2, y: obs.position.y)
            let recolor = SKAction.colorize(with: UIColor.red, colorBlendFactor: 1, duration: 2)
            obs.run(recolor)
            addMovement(obs)
            addChild(obs)
            break
        case .oneM:
            let obs = addObstacle(.medium)
            obs.position = CGPoint(x: self.size.width / 2, y: obs.position.y)
            let recolor = SKAction.colorize(with: UIColor.red, colorBlendFactor: 1, duration: 2)
            obs.run(recolor)
            addMovement(obs)
            addChild(obs)
            break
        case .oneL:
            let obs = addObstacle(.large)
            obs.position = CGPoint(x: self.size.width / 2, y: obs.position.y)
            let recolor = SKAction.colorize(with: UIColor.red, colorBlendFactor: 1, duration: 2)
            obs.run(recolor)
            addMovement(obs)
            addChild(obs)
            break
        case .twoS:
            let obs1 = addObstacle(.small)
            let obs2 = addObstacle(.small)
            let recolor = SKAction.colorize(with: UIColor.red, colorBlendFactor: 1, duration: 2)
            obs1.run(recolor)
            obs2.run(recolor)
            obs1.position = CGPoint(x: obs1.size.width + 50, y: obs1.position.y)
            obs2.position = CGPoint(x: self.size.width - obs2.size.width - 50, y: obs1.position.y)
            
            
            addMovement(obs1)
            addMovement(obs2)
            
            addChild(obs1)
            addChild(obs2)
            
            break
        case .twoM:
            let obs1 = addObstacle(.medium)
            let obs2 = addObstacle(.medium)
            
            obs1.position = CGPoint(x: obs1.size.width / 2 + 50, y: obs1.position.y)
            obs2.position = CGPoint(x: self.size.width - obs2.size.width / 2 - 50, y: obs1.position.y)
            let recolor = SKAction.colorize(with: UIColor.red, colorBlendFactor: 1, duration: 2)
            obs1.run(recolor)
            obs2.run(recolor)
            
            addMovement(obs1)
            addMovement(obs2)
            
            addChild(obs1)
            addChild(obs2)
            
            break
        case .threeS:
            let obs1 = addObstacle(.small)
            let obs2 = addObstacle(.small)
            let obs3 = addObstacle(.small)
            
            obs1.position = CGPoint(x: obs1.size.width / 2 + 50, y: obs1.position.y)
            obs2.position = CGPoint(x: self.size.width - obs2.size.width / 2 - 50, y: obs1.position.y)
            obs3.position = CGPoint(x: self.size.width / 2, y: obs1.position.y)
            
            let recolor = SKAction.colorize(with: UIColor.red, colorBlendFactor: 1, duration: 2)
            obs1.run(recolor)
            obs2.run(recolor)
            obs3.run(recolor)
            
            addMovement(obs1)
            addMovement(obs2)
            addMovement(obs3)
            
            addChild(obs1)
            addChild(obs2)
            addChild(obs3)
            
            break
            
        case .fourS:
            let obs1 = addObstacle(.small)
            let obs2 = addObstacle(.small)
            let obs3 = addObstacle(.small)
            let obs4 = addObstacle(.small)
            
            
            obs1.position = CGPoint(x: obs1.size.width / 2 + 50, y: obs1.position.y)
            obs2.position = CGPoint(x: self.size.width - obs2.size.width / 2 - 50, y: obs1.position.y)
            obs3.position = CGPoint(x: self.size.width / 2, y: obs1.position.y)
        
            obs4.position = CGPoint(x: self.size.width / 2 , y: obs1.position.y)
            
            let recolor = SKAction.colorize(with: UIColor.red, colorBlendFactor: 1, duration: 2)
            obs1.run(recolor)
            obs2.run(recolor)
            obs3.run(recolor)
            obs4.run(recolor)
            
            addMovement(obs1)
            addMovement(obs2)
            addMovement(obs3)
            addMovement(obs4)
            
            addChild(obs1)
            addChild(obs2)
            addChild(obs3)
            addChild(obs4)
            
            break
            
            
        }
    }
    

    override func didMove(to view: SKView) {
        self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        physicsWorld.contactDelegate = self
        let swipeRight:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(GameScene.swipedRight))
        swipeRight.direction = .right
        view.addGestureRecognizer(swipeRight)
        
        let swipeLeft:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(GameScene.swipedLeft))
        swipeLeft.direction = .left
        view.addGestureRecognizer(swipeLeft)
        scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        scoreLabel?.text = "Score: 0"
        scoreLabel?.fontColor = UIColor(red: 180/255, green: 10/255, blue: 30/255, alpha: 1)
        scoreLabel?.position = CGPoint(x: 260, y: 1800)
        scoreLabel?.horizontalAlignmentMode = .right
        scoreLabel?.fontSize = 50
        addChild(scoreLabel!)
       addPlayer()
        }
    

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        resetPlayer()
    }
    
    func resetPlayer(){
        player.position = playerPosition
    }
    
    func addRandomObsRow () {
        let randomNumber = Int(arc4random_uniform(6))
        
        addRowObs(RowType(rawValue: randomNumber)!)
    }
    
    var lastUpdateTimeInterval = TimeInterval()
    var lastYieldTimeInterval = TimeInterval()
    
    
    func updateWithTimeSinceLastUpdate (_ timeSinceLastUpdate:CFTimeInterval) {
        lastYieldTimeInterval += timeSinceLastUpdate
        if lastYieldTimeInterval > 0.6 {
            lastYieldTimeInterval = 0
            addRandomObsRow()
        }
    }

    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        var timeSinceLastUpdate = currentTime - lastUpdateTimeInterval
        lastUpdateTimeInterval = currentTime
        
        if timeSinceLastUpdate > 1 {
        timeSinceLastUpdate = 1/60
            lastUpdateTimeInterval = currentTime
        }
        
        updateWithTimeSinceLastUpdate(timeSinceLastUpdate)
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        if contact.bodyA.node?.name == "PLAYER" {
            print("GAME OVER")
            showGameOver()
        }
    }
    
    func showGameOver () {
        
        let label = SKLabelNode(text: "Game Over")
        label.fontName = "AvenirNext-Bold"
        label.fontSize = 60
        label.fontColor = UIColor.white
        label.position = CGPoint(x: self.frame.midX, y : self.frame.midY)
        self.addChild(label)
        self.backgroundColor = SKColor.black
        let delayInRestart = 5.0
        let transition = SKTransition.flipVertical(withDuration: 1.0)
        player.isHidden=true
        flag=true
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+delayInRestart){
        let gameScene = GameScene(fileNamed: "GameScene")
        let skView = self.view as SKView!
            gameScene?.scaleMode = .aspectFill
            skView?.presentScene(gameScene!, transition: transition)
        }
    }
}
